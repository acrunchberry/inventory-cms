<!-- Database -->
<?php include "includes/db.php"; ?>
<!-- Header -->
<?php include "includes/login-header.php"; ?>

<body class="hold-transaction login-page">

    <!-- Page Content -->
<div class="login-box">
  <div class="login-logo">
    <a href="login.php"><b>Admin</b>PDF</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="includes/fx_login.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" name="login" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!--<div class="social-auth-links text-center">-->
    <!--  <p>- OR -</p>-->
    <!--  <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using-->
    <!--    Facebook</a>-->
    <!--  <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using-->
    <!--    Google+</a>-->
    <!--</div>-->
    <!-- /.social-auth-links -->
    <!--<a href="#">I forgot my password</a><br>-->
    
    <a href="register.php" class="text-center">Register a new employee</a>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- Footer -->
<?php include"includes/login-footer.php"; ?>