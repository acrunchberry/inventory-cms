
<!-- Header -->
<?php include "includes/header.php"; ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!-- Top Navigation -->
<?php include "includes/topnav.php"; ?>
<!-- Side Navigation -->
<?php include "includes/sidebar.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Categories
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Categories</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
 
                          <div class="col-xs-4">  
                            <?php insert_categories(); ?> 
                             
                             <form action="" method="post">
                                 <div class="form-group">
                                     <label for="cat_title">Add Category</label>
                                     <input type="text" class="form-control" name="cat_title"/>    
                                 </div>
                                 <div class="form-group">
                                 <input class="btn btn-primary" type="submit" name="submit" value="Add Category"/>
                                 </div>
                             </form>
                             
                             <?php //UPDATE AND INCLUDE QUERY
                             if(isset($_GET['edit'])) {
                                $cat_id = $_GET['edit'];
                                include "includes/update_categories.php";
                             }
                             ?>
                          </div><!-- Add Category Form -->
                         <div class="col-xs-8">
                             <table class="table table-bordered table-hover">
                                 <thead>
                                     <tr>
                                         <th>Id</th>
                                         <th>Category Title</th>
                                         <th>Action</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <?php findAllCategories(); ?>
                                     
                                     <?php deleteCategories(); ?>
                                 </tbody>
                             </table>
                             
                         </div>
                         
             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!-- Footer -->
<?php include "includes/footer.php"; ?>
