<!-- Header -->
<?php include "includes/header.php"; ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
<!-- Top Navigation -->
<?php include "includes/topnav.php"; ?>
<!-- Side Navigation -->
<?php include "includes/sidebar.php"; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Phone Inventory
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Inventory</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
                <?php
                       if (isset($_SESSION["notification"]) && !empty($_SESSION["notification"])) {
                            $notification = $_SESSION["notification"];
                            echo "<div class='alert alert-success alert-dismissible' role='alert'>{$notification}</div>";
                            $_SESSION["notification"] = null;
                          }
                       
                        if(isset($_GET['source'])) {
                       
                        $source = $_GET['source'];       
                           
                       } else {
                       
                       $source = '';
                       
                           
                       }
                       switch($source) {
                           
                            case 'add_item';
                            include "includes/add_item.php"; 
                            break;
                            
                            case 'edit_item';
                            include "includes/edit_item.php";
                            break;
                               
                            default;
                            include "includes/view_all_items.php";   
                            break; 
                       }
               
                       ?>
             
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<!-- Footer -->
<?php include "includes/footer.php"; ?>
