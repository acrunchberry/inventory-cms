<?php

if(isset($_GET['p_id'])) {

    $the_item_id = $_GET['p_id'];    
    
}

$query = "SELECT * FROM items WHERE item_id = $the_item_id ";
$select_items_by_id = mysqli_query($connection, $query);
    
while($row = mysqli_fetch_assoc($select_items_by_id)) {
    $item_id = $row['item_id'];
    $item_category_id = $row['item_category_id'];
    $item_author = $row['item_author'];
    $item_title = $row['item_title'];
    $item_status = $row['item_status'];
    $item_image = $row['item_image'];
    $item_purchase_date = $row['item_purchase_date'];
    $item_serial = $row['item_serial'];
    $item_price = $row['item_price'];
    $item_retail = $row['item_retail'];
    $item_date = $row['item_date'];
    
    }
    
    if(isset($_POST['update_item'])) {
    
    $item_author = $_POST['item_author'];
    $item_title = $_POST['item_title'];
    $item_category_id = $_POST['item_category_id'];
    $item_status = $_POST['item_status'];
    $item_image = $_FILES['image']['name'];
    $item_image_temp = $_FILES['image']['tmp_name'];
    $item_purchase_date = $_POST['item_purchase_date'];
    $item_serial = $_POST['item_serial'];
    $item_price = $_POST['item_price'];
    $item_retail = $_POST['item_retail'];
    
    move_uploaded_file($item_image_temp, "../images/$item_image");
    
    if(empty($item_image)) 
    {
        
    $query = "SELECT * FROM items WHERE item_id = $the_item_id ";
    $select_image = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_array($select_image))
        {
        $item_image = $row['item_image'];
        }
    
    }
    
    $query = "UPDATE items SET ";
    $query .= "item_title = '{$item_title}', ";
    $query .= "item_category_id = '{$item_category_id}', ";
    $query .= "item_purchase_date = '{$item_purchase_date}', ";
    $query .= "item_author = '{$item_author}', ";
    $query .= "item_status = '{$item_status}', ";
    $query .= "item_price = '{$item_price}', ";
    $query .= "item_retail = '{$item_retail}', ";
    $query .= "item_serial = '{$item_serial}', ";
    $query .= "item_purchase_date = '{$item_purchase_date}', ";
    $query .= "item_image = '{$item_image}' ";
    $query .= "WHERE item_id = {$the_item_id} ";
    
    $update_item = mysqli_query($connection, $query);
    
    confirmQuery($update_item);
    
    $_SESSION["notification"] = "Item updated! <a href='../inventory.php' class='alert-link'>View Items</a>";
      header("Location: inventory.php");
    }
?>



<form action="" method="post" enctype="multipart/form-data">
    
    
    <div class="form-group col-md-4">
        <label for="item_status">Item Status</label>
        </br>
            <select name="item_status" id="">
                <option value="<?php echo $item_status ?>"><?php echo $item_status; ?></option>
                <?php
                    if($item_status = 'For Parts') {
                        echo "<option value='$item_status'>For Parts</option>";
                    } if($item_status = 'Ready') {
                        echo "<option value='$item_status'>Ready</option>";
                    } if($item_status = 'Sold') {
                        echo "<option value='$item_status'>Sold</option>";
                    } if($item_status = 'Prep') {
                        echo "<option value='$item_status'>Prep</option>";
                    } 
                ?>
            </select>
   
    </div>
    
    
    <div class="form-group col-lg-4">
        <label for="item_category">Model</label>
        </br>
        <select name="item_category_id" id="">
            
        <?php
            $query = "SELECT * FROM categories";
            $select_categories = mysqli_query($connection, $query);
            confirmQuery($select_categories);
            
            while($row = mysqli_fetch_assoc($select_categories)) {
            $cat_id = $row['cat_id'];
            $cat_title = $row['cat_title'];
            
            if($cat_id == $item_category_id) {
            
            echo "<option selected value='$cat_id'>{$cat_title}</option>";
            
            } else {
            echo "<option value='$cat_id'>{$cat_title}</option>";
            }
            }
        
        ?>
        </select>
    </div>
    
    <div class="form-group col-lg-4">
        <label for="item_author">Employee</label>
        </br>
        <select name="item_author" id="">
        <?php echo "<option value='{$item_author}'>{$item_author}</option>"; ?>
        <?php
        
        $query = "SELECT * FROM users";
        $select_authors = mysqli_query($connection, $query);
        
        confirmQuery($select_categories);
            
        while($row = mysqli_fetch_assoc($select_authors)) {
        $user_id = $row['user_id'];
        $username = $row['user_firstname'];
            
            echo "<option value='{$username}'>{$username}</option>";
            }
        
        ?>
        </select>
    </div>
    <div class="form-group col-lg-4">
        <label for="item_price">Purchase Price</label>
            <input value="<?php echo $item_price; ?>" type="text" class="form-control" name="item_price">
    </div>
    <div class="form-group col-lg-4">
        <label for="item_retail">Retail Price</label>
            <input value="<?php echo $item_retail; ?>" type="text" class="form-control" name="item_retail">
    </div>
    
    <div class="form-group col-lg-4">
        <label for="title">Item Name</label>
            <input value="<?php echo $item_title; ?>" type="text" class="form-control" name="item_title">
    </div>
          
    <div class="form-group col-md-4">
        <label for="item_serial">IMEI/ Serial #</label>
            <input value="<?php echo $item_serial; ?>" type="text" class="form-control" name="item_serial">
    </div>
    <div class="form-group col-md-4">
        <label for="tags">Purchase Date</label>
            <input type="date" value="<?php echo $item_purchase_date; ?>" class="form-control" name="item_purchase_date" placeholder="mm-dd-yyyy">
    </div>
    <div class="form-group col-md-4" name="item_image">
        <label for="item_title">Item Image</label>
        </br>
            <img width="100" src='../images/<?php echo "{$item_image}"; ?>' alt="">
            <input type="file" name="image">
    </div>
    
    <div class="form-group col-md-12">
        <input class="btn btn-primary" type="submit" name="update_item" value="Update Item">
    </div>
</form>