<?php

if(isset($_POST['create_user'])) {

$username = $_POST['username'];
$user_password = $_POST['user_password'];
$user_password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 10) );
$user_firstname = escape($_POST['user_firstname']);
$user_lastname = $_POST['user_lastname'];
$user_email = $_POST['user_email'];
$user_image = $_FILES['image']['name'];
$user_image_temp = $_FILES['image']['tmp_name'];
$user_role = $_POST['user_role'];
move_uploaded_file($user_image_temp, "../images/$user_image" );


$query = "INSERT INTO users(username, user_password, user_firstname, user_lastname, user_email, user_image, user_role) ";
$query .= "VALUES('{$username}','{$user_password}','{$user_firstname}','{$user_lastname}','{$user_email}','{$user_image}','{$user_role}' ) ";    
$create_user_query = mysqli_query($connection, $query);
confirmQuery($create_user_query);

echo "<div class='alert alert-success'>User Added!</div>" . " " . "<a href='users.php>View Users</a>";
}

?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="username">User Name</label>
            <input type="text" class="form-control" name="username">
    </div>
    <div class="form-group">
        <label for="user_password">User Password</label>
            <input type="password" class="form-control" name="user_password">
    </div>
    <div class="form-group">
        <label for="user_firstname">First Name</label>
            <input type="text" class="form-control" name="user_firstname">
    </div>
    
    <div class="form-group">
        <label for="user_lastname">Last Name</label>
            <input type="text" class="form-control" name="user_lastname">
    </div>
    <div class="form-group">
        <label for="user_email">User Email</label>
            <input type="email" class="form-control" name="user_email">
    </div>
    <div class="form-group col-md-12">
        <label for="image">Profile Image</label>
            <input type="file" name="image">
    </div>

     <div class="form-group">
        <label for="user_role">User Role</label>
        </br>
        <select name="user_role" id="">
            <option value="subscriber">Select Options</option>
            <option value="admin">Employee</option>
            <option value="subscriber">Admin</option>
        </select>
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_user" value="Add User">
    </div>
</form>