<form action="" method="post">
<table id="inventory" class="table table-bordered table-hover">
<div id="bulkOptionsContainer" class="col-xs-4">
        <select class="form-control" name="bulk_options" id="">
            <option value="">Select Options</option>
            <option value="Published">Publish</option>
            <option value="Draft">Draft</option>
            <option value="Delete">Delete</option>
            <option value="Clone">Clone</option>
        </select>
    </div>
    <div class="col-xs-4">
        <input type="submit" name="submit" class="btn btn-success" value="Apply"/>
            <a class="btn btn-primary" href="items.php?source=add_item">Add New</a>
    </div>
                <thead>
                <tr>
                  <th><input id="selectAllBoxes" type="checkbox" name=""/></th>
                  <th>Item Name</th>
                  <th>Model</th>
                  <th>IMEI/ Serial #</th>
                  <th>Status</th>
                  <th>Purchase Rep</th>
                  <th>Purchase Price</th>
                  <th>Purchase Date</th>
                  <th>Retail Price</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
  <?php
    // $query = "SELECT * FROM items ORDER BY item_id DESC";
    
    $query = "SELECT items.item_id, items.item_category_id, items.item_title, items.item_author, items.item_purchase_date, items.item_image, ";
    $query .= "items.item_serial, items.item_status, items.item_price, items.item_retail, items.item_sale_date, categories.cat_id, categories.cat_title ";
    $query .= "FROM items ";
    $query .= "LEFT JOIN categories ON items.item_category_id = categories.cat_id ORDER BY items.item_id DESC ";
    
    
    $select_items = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_assoc($select_items)) {
    $item_id            = $row['item_id'];
    $item_category_id   = $row['item_category_id'];
    $item_title         = $row['item_title'];  
    $item_author        = $row['item_author'];
    $item_purchase_date = $row['item_purchase_date'];
    $item_image         = $row['item_image'];
    $item_serial        = $row['item_serial'];
    $item_status        = $row['item_status'];
    $item_price         = $row['item_price'];
    $item_retail        = $row['item_retail'];
    $item_sale_date     = $row['item_views_count'];
    $cat_title          = $row['cat_title'];
    $cat_id             = $row['cat_id'];
    
    echo "<tr>";
    
    ?>
    
    <td><input class='checkBoxes' type='checkbox' name='checkBoxArray[]' value='<?php echo $item_id; ?>'></td>
    
    <?php
    echo "<td>{$item_title}</a></td>";
    echo "<td>{$cat_title}</td>";
    echo "<td>{$item_serial}</td>";
   
   if($item_status === 'Prep') {
    echo "<td class='bg-warning'>{$item_status}</td>";
} elseif($item_status === 'For Parts') {
    echo "<td class='bg-danger'>{$item_status}</td>";
} elseif($item_status === 'Ready') {
    echo "<td class='bg-success'>{$item_status}</td>";
} elseif($item_status === 'Sold') {
    echo "<td class='bg-primary'>{$item_status}</td>";
}
    echo "<td>{$item_author}</td>";
    echo "<td>&#36;{$item_price}</td>";
    echo "<td>{$item_purchase_date}</td>";
    echo "<td>&#36;{$item_retail}</td>";
    echo "<td>
                <a href='inventory.php?source=edit_item&p_id={$item_id}'>Edit</a><br>
                <a  rel='$item_id' href='' data-toggle='modal' data-target='#myModal' class='delete_link'>Delete</a>
          </td>";
    
    echo "</tr>";
   
    }

?>               
        </tbody>
            <tfoot>
                <tr>
                  <th><input id="selectAllBoxes" type="checkbox" name=""/></th>
                   <th>Item Name</th>
                  <th>Model</th>
                  <th>IMEI/ Serial #</th>
                  <th>Status</th>
                  <th>Purchase Rep</th>
                  <th>Purchase Price</th>
                  <th>Purchase Date</th>
                  <th>Retail Price</th>
                  <th>Action</th>
                </tr>
            </tfoot>
    </table>
</form>

<?php

if(isset($_GET['delete'])) {
$the_post_id = $_GET['delete'];

$query = "DELETE FROM items WHERE item_id = {$the_item_id} ";
$delete_query = mysqli_query($connection, $query);
header("Location: inventory.php");


} 

?>
<script>
    
    $(document).ready(function(){
       
       $(".delete_link").on('click', function(){
          
           var id = $(this).attr("rel");
           
           var delete_url = "inventory.php?delete="+ id +" ";
           
           $(".modal_delete_link").attr("href", delete_url);
         
       });
        
    });
</script> 
               
                
                
                
                
                
               