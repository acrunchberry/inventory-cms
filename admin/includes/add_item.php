<?php

if(isset($_POST['create_item'])) {
    
$item_title = $_POST['item_title'];
$item_author = $_POST['item_author'];
$item_category_id = $_POST['item_category_id'];
$item_status = $_POST['item_status'];

$item_image = $_FILES['image']['name'];
$item_image_temp = $_FILES['image']['tmp_name'];

$item_serial = $_POST['item_serial'];
$item_price = $_POST['item_price'];
$item_date = $_POST['item_purchase_date'];
// $item_comment_count = 4;
    
move_uploaded_file($item_image_temp, "../images/$item_image" );

$query = "INSERT INTO items(item_category_id, item_title, item_author, item_purchase_date, item_image, item_price, item_serial, item_status) ";
$query .= "VALUES({$item_category_id},'{$item_title}','{$item_author}','{$item_date}','{$item_image}','{$item_price}','{$item_serial}', '{$item_status}' ) ";    

$create_item_query = mysqli_query($connection, $query);

confirmQuery($create_item_query);
$the_item_id = mysqli_insert_id($connection);

echo "<p class='bg-success'>Item Added. <a href='../inventory.php?p_id={$the_item_id}'>View item</a> ";
}

?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group col-md-3">
        <label for="tags">Item Name</label>
            <input type="text" class="form-control" name="item_title" placeholder="Device Name">
    </div>
    <div class="form-group col-md-3">
        <label for="item_category">Model</label>
        </br>
        <select name="item_category_id" id="">
            
        <?php
        
        $query = "SELECT * FROM categories";
        $select_categories = mysqli_query($connection, $query);
        
        confirmQuery($select_categories);
            
        while($row = mysqli_fetch_assoc($select_categories )) {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
            
            echo "<option value='$cat_id'>{$cat_title}</option>";
            }
        
        ?>
        </select>
    </div>
    
    <div class="form-group col-md-3">
        <label for="author">Purchasing Employee</label>
        </br>
        <select name="item_author" id="">
        <?php
        
        $query = "SELECT * FROM users";
        $select_authors = mysqli_query($connection, $query);
        
        confirmQuery($select_authors);
            
        while($row = mysqli_fetch_assoc($select_authors)) {
        $user_id = $row['user_id'];
        $username = $row['user_firstname'];
            
            echo "<option value='{$username}'>{$username}</option>";
            }
        
        ?>
        </select>
    </div>
    
    <div class="form-group col-md-3">
        <label for="item_status">Item Status</label>
        </br>
            <select name="item_status" id="">
                <option value="Draft">--Select Status--</option>
                <option value="Prep">Prep</option>
                <option value="For Parts">For Parts</option>
                <option value="Ready">Ready</option>
                <option value="Sold">Sold</option>
            </select>
    </div>
    
    <div class="form-group col-md-12">
        <label for="image">Item Image</label>
            <input type="file" name="image">
    </div>
    <div class="form-group col-md-4">
        <label for="tags">Item IMEI/ Serial #</label>
            <input type="text" class="form-control" name="item_serial" placeholder="Use *#06# for IMEI">
    </div>
    <div class="form-group col-md-4">
        <label for="tags">Item Purchase Price</label>
            <input type="text" class="form-control" name="item_price" placeholder="$0.00">
    </div>
   <div class="form-group col-md-4">
        <label for="tags">Item Purchase Date</label>
            <input type="date" class="form-control" name="item_purchase_date" placeholder="mm-dd-yyyy">
    </div>
    
    <div class="form-group col-md-12">
        <input class="btn btn-primary" type="submit" name="create_item" value="Add item">
    </div>
</form>