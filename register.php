<!-- Database -->
<?php include "includes/db.php"; ?>
<!-- Header -->
<?php include "includes/login-header.php"; ?>

<?php
if(isset($_POST['register'])) {

$username = escape($_POST['username']);
$email    = escape($_POST['email']);
$password = escape($_POST['password']);

if(!empty($username) && !empty($email) && !empty($password)) {

$username = escape($username);
$email    = escape($email);
$password = escape($password);

$password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12) );

$query = "INSERT INTO users (username, user_email, user_password, user_role) ";
$query .= "VALUES('{$username}','{$email}','{$password}','subscriber' )";
$register_user_query = mysqli_query($connection, $query);
if(!$register_user_query) {
    die("QUERY FAILED " . mysqli_error($connection));
}
$message = "<h5 class='alert alert-success'>Your registration has been submitted.</h5>";
} else {
$message = "<h5 class='alert alert-danger'>Fields cannot be empty</h5>"; 
}
} else {
$message = "";
}  

?>



<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="index.php"><b>Admin</b>PDF</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Register a new employee</p>

    <form action="register.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" name="register" class="btn btn-primary btn-block btn-flat">Register</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <a href="login.php" class="text-center">I already have a membership</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- Footer -->
<?php include"includes/login-footer.php"; ?>